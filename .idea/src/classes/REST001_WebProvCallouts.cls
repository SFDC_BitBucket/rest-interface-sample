public class REST001_WebProvCallouts {

    /* **** GET USERS **** */
    public static HTTPResponse getChangedUsersByWebProvService(){
        
        //GET Users
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:DRD/changes?count=50');
        req.setMethod('GET');
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        return res;
    }
}