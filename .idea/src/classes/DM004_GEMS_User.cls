/**
* Created by PA_VE on 05.06.2018.
*/

public class DM004_GEMS_User {
    
    public static void getChangedGemsUsers(HTTPResponse users){
        
        System.debug('response ' + users.getBody());
        
        
        //Start Process the serialization of the user changes
        Map<String, Object> responseBodyElement = (Map<String, Object>) JSON.deserializeUntyped(users.getBody());
        
        List<Object> responseListWrapperElement = (List<Object>) responseBodyElement.get('changeRecords');
        
        List<GEMS_UserVO> serializedUsers = new List<GEMS_UserVO>();
        
        for(Object user : responseListWrapperElement){
            serializedUsers.add(GEMS_UserVO.parse(JSON.serialize(user)));
        }
        
        System.debug(serializedUsers);
        
        for(GEMS_UserVO g : serializedUsers ){
            
            //PROCESS
            
        }
        
    }
    
}