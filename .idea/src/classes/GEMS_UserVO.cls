public class GEMS_UserVO {

    public String type_Z {get;set;} // in json: type
    public String timestamp {get;set;}
    public OldAttrs oldAttrs {get;set;}
    public OldAttrs newAttrs {get;set;}
    public Integer changeId {get;set;}

    public GEMS_UserVO(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'type') {
                        type_Z = parser.getText();
                    } else if (text == 'timestamp') {
                        timestamp = parser.getText();
                    } else if (text == 'oldAttrs') {
                        oldAttrs = new OldAttrs(parser);
                    } else if (text == 'newAttrs') {
                        newAttrs = new OldAttrs(parser);
                    } else if (text == 'changeId') {
                        changeId = parser.getIntegerValue();
                    } else {
                        System.debug(LoggingLevel.WARN, 'GEMS_UserVO consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }

    public class OldAttrs {
        public List<String> dcxCompanyID {get;set;}
        public List<String> departmentNumber {get;set;}
        public List<String> facsimileTelephoneNumber {get;set;}
        public List<String> c {get;set;}
        public List<String> mail {get;set;}
        public List<String> telephoneNumber {get;set;}
        public List<String> sn {get;set;}
        public List<String> givenName {get;set;}
        public List<String> dcxIapEntGrps {get;set;}
        public List<String> dcxIapAuthGrps {get;set;}
        public List<String> uid {get;set;}

        public OldAttrs(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'dcxCompanyID') {
                            dcxCompanyID = arrayOfString(parser);
                        } else if (text == 'departmentNumber') {
                            departmentNumber = arrayOfString(parser);
                        } else if (text == 'facsimileTelephoneNumber') {
                            facsimileTelephoneNumber = arrayOfString(parser);
                        } else if (text == 'c') {
                            c = arrayOfString(parser);
                        } else if (text == 'mail') {
                            mail = arrayOfString(parser);
                        } else if (text == 'telephoneNumber') {
                            telephoneNumber = arrayOfString(parser);
                        } else if (text == 'sn') {
                            sn = arrayOfString(parser);
                        } else if (text == 'givenName') {
                            givenName = arrayOfString(parser);
                        } else if (text == 'dcxIapEntGrps') {
                            dcxIapEntGrps = arrayOfString(parser);
                        } else if (text == 'dcxIapAuthGrps') {
                            dcxIapAuthGrps = arrayOfString(parser);
                        } else if (text == 'uid') {
                            uid = arrayOfString(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'OldAttrs consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }


    public static GEMS_UserVO parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new GEMS_UserVO(parser);
    }

    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT ||
                    curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                    curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }





    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }


}