public class SM008_WebProvisioningServiceCallouts {
    
    /* Method to process the Interface Callouts */
    
	
    // Gets the changed User Records
    @future(Callout=true)
    public static void getChangedUsers(){

        Boolean finishedProcess = false;
        Integer lastProcessedInitId;
        Integer numberOfChangeRecords;
        
        // successfull, if body has no content
            
            HTTPResponse getChangedUserResponse;
            
            // Get Users
            if(!Test.isRunningTest()){
                getChangedUserResponse = REST001_WebProvCallouts.getChangedUsersByWebProvService();
            }else{
                getChangedUserResponse = MockupFactory.getChangedUser();
            }
            
            // Get last RecordId & Status of Completion
            Map<String, Object> responseParameter = (Map<String, Object>) JSON.deserializeUntyped(getChangedUserResponse.getBody());
            finishedProcess = (Boolean) responseParameter.get('listComplete');
            lastProcessedInitId = (Integer) responseParameter.get('lastProcessedChangeId');
            numberOfChangeRecords = (Integer) responseParameter.get('numberOfChangeRecords');
  
            //Import Users
            if(numberOfChangeRecords > 0){
                DM004_GEMS_User.getChangedGemsUsers(getChangedUserResponse);
            }

             
    }
}